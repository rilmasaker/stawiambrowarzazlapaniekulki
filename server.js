const express = require('express');
const app = express();

const toTest = {strategy: 'local'};

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/config', (req, res) => res.json(toTest));

app.listen(7080, () => console.log('poszlo'));