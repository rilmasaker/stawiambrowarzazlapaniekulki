'use strict';
module.exports = (sequelize, DataTypes) => {
  var storageStrategies = sequelize.define('storageStrategies', {
    strategies: DataTypes.STRING
  }, {});
  storageStrategies.associate = function(models) {
    // associations can be defined here
  };
  return storageStrategies;
};