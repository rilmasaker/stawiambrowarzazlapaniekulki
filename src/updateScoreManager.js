import $ from 'jquery';
import { soundManager } from './soundManager';
import { store } from './store'

class UpdateScoreManager {
  constructor() {
    this.$scoreDisplay = $('.score');
    this.$livesDisplay = $('.lives');
  }

updateScore() {

  store.subscribe(() => {
    this.view = store.getState();
    this.$scoreDisplay.text(this.view.score);
    this.$livesDisplay.text(this.view.lives)

  })

}
}

export const updateScoreManager = new UpdateScoreManager();
