import './css/main.css';
import $ from 'jquery';
import { PopupInterface } from './popupInterface';
import { UserInterface } from './userInterface';
import { soundManager } from './soundManager';
import { setStorageStrategy } from './score-service';
import { updateScoreManager } from './updateScoreManager';
import { getConfig } from './server/config';


const $document = $(document);

$document.ready(function () {

  getConfig();
  let popupInterface = new PopupInterface();
  let userInterface = new UserInterface();
  updateScoreManager.updateScore();
});

