let storageStrategies = {
  local: {
    set: null,
    get: null
  },
  db: {
    set: null,
    get: null
  }
};

(function () {
  storageStrategies.local.set = (key, item) =>
    localStorage.setItem(key, item);
})();

(function () {
  storageStrategies.local.get = (key) => {
    const data = localStorage.getItem(key);
    return data;
  }
})();

function getStorage() {
  return storageStrategies[decisionFromServer];
}

let decisionFromServer = 'local';
export function setStorageStrategy(strategy) {
  decisionFromServer = strategy;
}

class ScoreService {
  save(name, score) {
    return new Promise(resolve => {
      let scoreObject = {
        name: name,
        score: score
      };
      let scoreArray = JSON.parse(localStorage.getItem('scoreTable'));
      scoreArray.push(scoreObject);
      localStorage.setItem('scoreTable', JSON.stringify(scoreArray));
      resolve();
    });
  }

  get() {
    return new Promise(resolve => {
      let scoreArray = getStorage().get('scoreTable');
      if (scoreArray === null) {
        scoreArray = [];
      } else {
        scoreArray = JSON.parse(scoreArray).sort((a, b) => b.score - a.score);
      }
      resolve(scoreArray);
    });
  }

  save(name, score) {
    return new Promise(resolve => {

      let scoreObject = {
        name: name,
        score: score
      };
      let scoreArray = JSON.parse(getStorage().get('scoreTable'));
      scoreArray.push(scoreObject);
      getStorage().set('scoreTable', JSON.stringify(scoreArray));
      resolve();
    });
  }
}

export const scoreService = new ScoreService();