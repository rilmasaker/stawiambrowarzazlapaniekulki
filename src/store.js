import { createStore } from 'redux';
import reducer from './reducers/combineReducers';

export const store = createStore(reducer);
