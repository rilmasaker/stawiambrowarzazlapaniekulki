import $ from 'jquery';
import { soundManager } from './soundManager';
import { scoreService } from './score-service';
import { store } from './store'

export class PopupInterface {
  constructor() {
    //TABLECLICK
    this.$ShowTable = $('.ShowTableButton');
    this.$table = $('.table');
    this.$fisrtName = $('.firstName');
    this.$firstScore = $('.firstScore');
    this.$SecoundName = $('.SecoundName');
    this.$SecoundScore = $('.SecoundScore');
    this.isDisplayTable = false;
    //SAVECLICK
    this.$save = $('.save');
    this.$popup = $('.popup');
    this.scoreTable = [];
    this.$area = $('.area');
    this.$ball = $('.ball');

    this.subscribeEvents();
  }

  displayAndSortTable() {
    if (!this.isDisplayTable) {
      scoreService.get().then((scoreArray) => {
        this.$fisrtName.text(scoreArray[0].name);
        this.$firstScore.text(scoreArray[0].score);
        this.$SecoundName.text(scoreArray[1].name);
        this.$SecoundScore.text(scoreArray[1].score);

        this.isDisplayTable = true;
        this.$table.addClass('ShowMeTable');
        soundManager.playDispalyTableSound();
      });
    } else {
      this.isDisplayTable = false;
      this.$table.removeClass('ShowMeTable');
    }
  }
  updateTableAndReload() {
    let name = $('.name').val();
    let score = store.getState().score;

    if (name === "") {
      this.$popup.removeClass('visible');
      setTimeout(() => window.location.reload(), 2200);
    } else {
      scoreService.save(name, score).then();
    }
    soundManager.playSaveSound();
    this.$popup.removeClass('visible');
    setTimeout(() => window.location.reload(), 2200);
  }


  subscribeEvents() {
    this.$ShowTable.on('click', event => {
      event.stopPropagation();
      this.displayAndSortTable();

    });
    this.$save.on('click', event => {
      event.stopPropagation();
      this.updateTableAndReload();
    });
  }
}
