import $ from 'jquery';
import { soundManager } from './soundManager';
import { store } from './store'

export class AreaInterface {
  constructor() {
    this.$document = $(document);
    this.$area = $('.area');
    this.subscribeEvents();
    this.$window = $(window);
    this.currentMousePos = { x: -1, y: -1 };
    this.$gun_1 = $('.gun_l');
    this.$gun_2 = $('.gun_2');
    this.currentMousePos = { x: -1, y: -1 };
  }

  areaClick() {
    store.dispatch({type: 'DECREMENT'});
    soundManager.playMissSound();
  }

  trackMouse(event) {
    this.currentMousePos.x = event.pageX;
    this.currentMousePos.y = event.pageY;

    let width = Number(this.$window.width());
    let height = Number(this.$window.height());
    let half_w = width / 2;
    let half_h = height / 2;
    let left_pos_l = ((this.currentMousePos.x / (width * 2)) * 50) - 0;
    let right_pos_2 = ((this.currentMousePos.x / (width * 2)) * 50) + 25;
    let top_pos_l = (this.currentMousePos.y);
    let top_pos_2 = (this.currentMousePos.y);

    this.$gun_1.css('left', left_pos_l).css('top', top_pos_l);
    this.$gun_2.css('left', right_pos_2).css('top', top_pos_2);
  }

  subscribeEvents() {
    this.$document.on('mousemove', (event) => this.trackMouse(event));
    this.$area.on('click', event => {
      event.stopPropagation();
      this.areaClick();
    });
  }
}








