import $ from 'jquery';
import { Ball } from './ball';


export class BallControler {
  constructor(numberOfBall) {
    this.howManyBalls(numberOfBall);
  }

  howManyBalls(numberOfBall) {
    this.balls = [];
    for (let i = 0; i < numberOfBall; i++) {
      this.balls.push(new Ball());
    }

  }

}