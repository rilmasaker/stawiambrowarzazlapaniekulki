import $ from 'jquery';
import { soundManager } from '../soundManager';
import { updateScoreManager } from '../updateScoreManager';
import { store } from '../store'

export class Ball {
  constructor() {
    this.$ball = $('<div class="ball" ></div>')
    this.$area = $('.area');
    this.movingBall();

    this.$area.append(this.$ball);
    this.subscribeEvents();
    this.isBallOn = true;

    this.$displayBallClicks = $('.ballClick');
  }

  makeNewPosition() {
    let height = this.$area.height() - 100;
    let width = this.$area.width() - 100;
    let newHeight = Math.floor(Math.random() * height);
    let newWidth = Math.floor(Math.random() * width);
    return [newHeight, newWidth];
  }

  movingBall() {
    this.$ball.addClass("ball");
    let newPos = this.makeNewPosition();
    this.$ball.animate({ top: newPos[0], left: newPos[1] }, 1200, () => {
      this.movingBall();
    });
    if (!this.isBallOn) {
      return;
    }
  }

  destroy() {
    this.isBallOn = false;
    this.$ball.remove();
  }


  handleBallClick() {
    store.dispatch({type: 'INCREMENT'})
    soundManager.playRemovefakeBallsSound();
  //   updateScoreManager.cnn();
  // 
}

  subscribeEvents() {
    this.$ball.on('click', event => {
      event.stopPropagation();
      this.handleBallClick(event); 
      this.destroy();

    });
  }
}

