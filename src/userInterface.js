import $ from 'jquery';
import {soundManager} from './soundManager';
import {BallControler} from './ball/ballControler';
import {AreaInterface} from './areaInterface';
import {store} from './store'


export class UserInterface {
  constructor() {
    this.$popup = $('.popup');
    this.$area = $('.area');

    this.$stop = $('.stop');
    this.$mute = $('.mute');
    this.$start = $('.start');
    this.$input = $('.numberOfBalls');

    this.subscribeEvents();

    this.scoreTable = [{
      name: 'AdminIsBlind',
      score: -100
    }, {
      name: 'noname',
      score: -100
    }];
  }

  timeCounter() {
    setInterval(() => {
      this.timeToLeft = $('.time').val();
      this.display = this.timeToLeft + 1;
      $('.time').val(this.display);

      if (this.timeToLeft === 29) {
        soundManager.playStopSound();
        this.stopGame();
      } else if (this.timeToLeft === 15) {
        this.$ball = $('.ball');
        this.$ball.addClass('smallBalls');
        soundManager.playSmallBallsClickSound();
      }

    }, 1000)
  }

  zeroLivesStop() {
    if (this.lives === 0) {
      this.$popup.addClass('visible');
      this.$area.addClass('hide');
    }
  }

  stopGame() {
    this.$ball = $('.ball');
    this.$popup.addClass('visible');
    this.$area.addClass('hide');


    soundManager.playStopSound();
    if (localStorage.scoreTable === undefined) {
      localStorage.setItem('scoreTable', JSON.stringify(this.scoreTable));
    }

  }
  startClick() {
    
    if (localStorage.scoreTable === undefined) {
      localStorage.setItem('scoreTable', JSON.stringify(this.scoreTable));
    }



    soundManager.playStartSound();

    this.timeCounter();

    this.ballControler = new BallControler(20);
    this.areaInterface = new AreaInterface();

    this.$start.addClass('hide');
  }

  subscribeEvents() {

    this.$mute.on('click', event => {
      event.stopPropagation();
      soundManager.toggleMuteMusic(event);
    });

    this.$start.one('click', event => {
      event.stopPropagation();
      this.startClick();
    });

    this.$stop.on('click', event => {
      event.stopPropagation();
      this.stopGame();
    });

    store.subscribe(() => {
      const newState = store.getState();
      this.lives = newState.lives;
      this.zeroLivesStop();
    });
  }
}