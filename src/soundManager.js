import $ from 'jquery';
class SoundManager {
  constructor() {
    this.isMuted = false;
    this.backgroundMusic = new Audio("http://www.pacdv.com/sounds/ambience_sounds/metro-station-1.mp3");
    this.missSound = new Audio('http://www.pacdv.com/sounds/interface_sound_effects/sound5.mp3');
    this.removeBallsSound = new Audio('http://www.pacdv.com/sounds/people_sound_effects/ahhh.mp3');
    this.startSound = new Audio('http://www.pacdv.com/sounds/machine_sound_effects/electric-drill-1.mp3');
    this.stopSound = new Audio('http://www.pacdv.com/sounds/transportation_sounds/siren.mp3');
    this.smallBallsClickSound = new Audio('http://www.pacdv.com/sounds/mechanical_sound_effects/clong_1.mp3');
    this.saveSound = new Audio('http://www.pacdv.com/sounds/machine_sound_effects/electric-drill-2.mp3');
    this.dispalyTableSound = new Audio('http://www.pacdv.com/sounds/people_sound_effects/crowd-excited.mp3');
    this.backgroundMusic.loop = true;
    this.backgroundMusic.play();
  }
  
  toggleMuteMusic() {
    if (!this.isMuted) {
      this.isMuted = true;
      this.backgroundMusic.pause();

    } else {
      this.isMuted = false;
      this.backgroundMusic.play();
    }
  }


  playMissSound() {
    this.missSound.play();
  }
  playRemovefakeBallsSound() {
    this.removeBallsSound.play();
  }

  playfakeBallsSound() {
    this.fakeBallsSound.play();
  }
  playStopSound() {
    this.stopSound.play();
  }

  playSmallBallsClickSound() {
    this.smallBallsClickSound.play();
  }

  playSaveSound() {
    this.saveSound.play();
  }

  playStartSound() {
    this.startSound.play();
  }

  playDispalyTableSound() {
    this.dispalyTableSound.play();
  }

}

export const soundManager = new SoundManager();
