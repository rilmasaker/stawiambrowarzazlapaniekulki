export const livesReducer = (state = 5, action)=> {
  switch (action.type) {
    case 'DECREMENT':
      return state - 1;
    default:
      return state;
  }
}