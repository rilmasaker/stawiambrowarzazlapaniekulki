import { combineReducers } from 'redux';

import { pointsReducer } from './pointsReducer';
import { livesReducer } from './livesReducer';

export default combineReducers({
  score: pointsReducer,
  lives: livesReducer
})
