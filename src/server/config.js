import $ from 'jquery';

import { setStorageStrategy } from '../score-service';


export function getConfig() {
    return   $.get('http://localhost:7080/api/config/local',(data) => {
        setStorageStrategy(data);
    });
}

