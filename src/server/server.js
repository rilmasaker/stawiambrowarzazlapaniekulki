// import { request } from 'http';

const express = require('express');
const Sequelize = require('sequelize');
const bodyParser = require('body-parser');
const models = require('../../database/models');

const app = express();

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.put('/api/config', (request, response) => {
  models.storageStrategies.create(request.body).then(user => response.json(user));

});

app.get('/api/config', (request, response) => {

  models.storageStrategies.findAll({ raw: true }).then(config => {
    response.json(config)
  });
});

app.get('/api/config/:name', (request, response) => {

  models.storageStrategies.findOne({ 
    where:{
      strategies:request.params.name
    },
    raw: true }).then(config => {
    response.json(config.strategies)
  });
});



models.sequelize.sync().then(() => {
  app.listen(7080, () => console.log('poszlo'));
});
