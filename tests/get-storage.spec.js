import expect from 'expect';
import sinon from 'sinon';

import { scoreService } from '../src/score-service';

describe('get storage', () => {

  const origlocalStorage = global.localStorage;
  let dbStorage = {};
  beforeEach(() => {
    global.localStorage = {
      setItem: sinon.spy(function (key, item) {
        dbStorage[key] = item;
      }),
      getItem: sinon.spy(function (key) {
        return dbStorage[key] === void 0 ? null : dbStorage[key];
      })
    };
  });

  afterEach(() => {
    dbStorage = {};
    global.localStorage = origlocalStorage;
  });



  it('get should return promise', () => {
    expect(scoreService.get() instanceof Promise).toEqual(true);
  });

  it('should resolve promise with array', done => {
    scoreService.get().then(scoreArray => {
      expect(scoreArray).toEqual([])
      done();
    });
  });

  it('should call local storage', done => {
    scoreService.get().then(() => {
      expect(global.localStorage.getItem.calledOnce).toEqual(true);
      done();
    });

  });
  it('save hould return promise', () => {
    expect(scoreService.save() instanceof Promise).toEqual(true);
  });

  it('i dont know what happend', done => {
    scoreService.save(null, null).then(() => {
      expect({}).toEqual({})
      done();
    });
  });
  it('i dont know what happend with undefined', done => {
    scoreService.save("t", 0).then(() => {
      expect({}).toEqual({})
      done();
    });
  });

});
