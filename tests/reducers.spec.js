import expect from 'expect';
import deepFreeze from 'deep-freeze';

import { livesReducer } from '../src/reducers/livesReducer';
import { pointsReducer } from '../src/reducers/pointsReducer';

describe('livesReducer', () => {

  it('should decrese lives', () => {
    let action = { type: 'DECREMENT' };
    deepFreeze(action);
    expect(livesReducer(2, action)).toEqual(1);
  })
  it('shouldnt change state', () => {
    let action = { type: 'kokoJumbo' };
    deepFreeze(action);
    expect(livesReducer(2, action)).toEqual(2);
  })

})
describe('pointsReducer', () => {

  it('sholud increse points', () => {

    let action = { type: 'INCREMENT' };
    deepFreeze(action);
    expect(pointsReducer(2, action)).toEqual(3);
  })
  it('shouldnt change state', () => {
    let action = { type: 'kokoJumbo' };
    let stateBefore = 2
    let stateAfter = stateBefore;
    deepFreeze(action);
    expect(livesReducer(stateBefore, action)).toEqual(stateAfter);
  })

})